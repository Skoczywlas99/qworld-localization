# QWorld Localization


## Info

This repository manages the translations into different languages of the QWorld tutorials:

- [bronze-qiskit](https://gitlab.com/qworld/bronze-qiskit)
  - Bengali ([bn](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/bn))
  - Chinese, Simplified ([zh_CN](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/zh_CN))
  - Chinese, Traditional ([zh_TW](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/zh_TW))
  - Czech ([cs](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/cs))
  - Dutch ([nl](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/nl))
  - Filipino ([fil](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fil))
  - French ([fr](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fr))
  - German ([de](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/de))
  - Persian ([fa](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fa))
  - Polish ([pl](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/pl))
  - Spanish ([es](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/es))

- [nickel](https://gitlab.com/qworld/nickel)
  - Spanish ([es](https://gitlab.com/clausia/qworld-localization/-/tree/main/nickel/es))

- [silver](https://gitlab.com/qworld/silver)
